/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.springframework.samples.petclinic.citas;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.samples.petclinic.appointment.Appointment;
import org.springframework.samples.petclinic.appointment.AppointmentRepository;
import org.springframework.samples.petclinic.owner.OwnerRepository;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Faviel
 */

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/api")

public class CitasApiController {
    
    private final AppointmentRepository appointmentRepository;
    private final OwnerRepository owners;
    private final EspecialidadesRepository especialedadesRepo;
    private final MascotasRepository mascotasRepository;
    private final LoginRepository loginRepository;


    
    public CitasApiController(AppointmentRepository appointmentRepository, OwnerRepository owners, EspecialidadesRepository esp, MascotasRepository mascotasRepository, LoginRepository loginRepository) {
        this.appointmentRepository = appointmentRepository;
        this.owners = owners;
        this.especialedadesRepo = esp;
        this.mascotasRepository = mascotasRepository;
        this.loginRepository = loginRepository;
    }
    
    /**
    * Get all citas list.
    *
    * @return the list
    */
    
    //Login
    @RequestMapping(method = RequestMethod.GET, path = "/login1")
    public ArrayList<Login> getLogin(){
        ArrayList<Login> login = this.loginRepository.All();
        return login; 
    }
    
    @GetMapping("login/{username}/{password}")
    public Integer getName(@PathVariable("username") String username, @PathVariable("password") String password){
        Login username1 = this.loginRepository.ValidByUsername(username);
        Login password1 = this.loginRepository.ValidByPassword(password);
        
        if (username1.equals(password1)){
            return username1.getId();
        }else{
            return 0;
        }
            
        
        
    }
    
    @GetMapping("/citas")
    public Collection<Appointment> getCitas() {
        Collection<Appointment> citas = this.appointmentRepository.getAppointments();
        return citas;
    }
    
    @GetMapping("/citas/{id}")
     public Appointment cita(@PathVariable Integer id) {
      return this.appointmentRepository.findById(id);
     }
    
   @PostMapping("/citas/new")
    public Appointment store(@Valid @RequestBody Appointment cita) {
        
      this.appointmentRepository.save(cita);
        return cita;
     }
    
    @PostMapping("/citas/delete/{id}")
    public String delete(@PathVariable Integer id) {
      Appointment cita =  this.appointmentRepository.findById(id);
      this.appointmentRepository.delete(cita);
        return "la cita ha sido eliminada";
    }
}
