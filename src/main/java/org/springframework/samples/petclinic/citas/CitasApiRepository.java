/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.springframework.samples.petclinic.citas;

import java.util.Collection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Faviel
 */

@Repository
public interface CitasApiRepository extends JpaRepository<Citas, Integer>{
    
    @Query("SELECT cita FROM Citas cita")
    @Transactional(readOnly = true)
    Collection<Citas>getCitas();
    
//    void save(Citas citas);  
}
