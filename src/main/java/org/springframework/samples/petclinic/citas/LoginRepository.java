/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.springframework.samples.petclinic.citas;

import java.util.ArrayList;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Faviel
 */
public interface LoginRepository extends Repository<Login, Integer>{
    
    void save(Login login);
       
    @Query("SELECT login FROM Login login ")
    @Transactional(readOnly = true)
    ArrayList<Login> All();
   
    @Query("SELECT login FROM Login login WHERE login.username =:username")
    @Transactional(readOnly = true)
    Login ValidByUsername(@Param("username") String username);
    
    @Query("SELECT login FROM Login login WHERE login.password = :password")
    @Transactional(readOnly = true)
    Login ValidByPassword(@Param("password") String password);
}
