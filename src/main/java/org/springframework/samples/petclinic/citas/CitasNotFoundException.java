/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.springframework.samples.petclinic.citas;

/**
 *
 * @author Faviel
 */
class CitasNotFoundException extends RuntimeException {

  CitasNotFoundException(Integer id) {
    super("No se puedo encontrar la cita " + id);
  }
}
